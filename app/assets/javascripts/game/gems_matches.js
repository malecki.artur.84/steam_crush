Game.GemsMatches = function(board) {
  this.board = board;
  this.matches = [];
  this.level = this.board.level;
}

Game.GemsMatches.prototype.fetch = function() {
  this.matches = [];

  this.bombsCheck();
  this.defaultCheck();

  return this.matches;
}
Game.GemsMatches.prototype.activateBomb = function(x, y) {
  var hits = [],
      level = this.board.level,
      xIndex, yIndex, gem;

  for(xIndex = 0; xIndex < level.xSize; xIndex++) {
    gem = this.board.getGem(xIndex, y);
    hits.push(gem);
  }
  for(yIndex = 0; yIndex < level.ySize; yIndex++) {
    gem = this.board.getGem(x, yIndex);
    hits.push(gem);
  }

  this.matches.push({ type: 'bomb', hits: hits });
}

Game.GemsMatches.prototype.bombsCheck = function() {
  var x, y, gem;

  for(x = 0; x < this.level.xSize; x++) {
    for(y = 0; y < this.level.ySize; y++) {
      gem = this.board.getGem(x, y);
      if(gem && gem.isBomb()) {
        var searchResultXR = { frame: undefined, size: 0 },
            searchResultXL = { frame: undefined, size: 0 },
            searchResultYT = { frame: undefined, size: 0 },
            searchResultYB = { frame: undefined, size: 0 };

        this.bombMatch(searchResultXR, x, y, 'x', 'r');
        this.bombMatch(searchResultXL, x, y, 'x', 'l');
        this.bombMatch(searchResultYT, x, y, 'y', 't');
        this.bombMatch(searchResultYB, x, y, 'y', 'b');

        if(searchResultXL.frame === searchResultXR.frame) {
          this.activateBomb(x, y);
          break;
        } else {
          if(searchResultXL.size >= 2 || searchResultXR.size >= 2) {
            this.activateBomb(x, y);
            break;
          }
        }

        if(searchResultYT.frame === searchResultYB.frame) {
          this.activateBomb(x, y);
          break;
        } else {
          if(searchResultYT.size >= 2 || searchResultYB.size >= 2) {
            this.activateBomb(x, y);
            break;
          }
        }
      }
    }
  }
},

Game.GemsMatches.prototype.bombMatch = function(searchResult, x, y, axis, direction) {
  var nextX, nextY, nextGem, borderCondition;

  if(axis === 'x') {
    nextY = y;
    if(direction === 'r') {
      nextX = x + 1;
      borderCondition = nextX < this.level.xSize;
    } else {
      nextX = x - 1;
      borderCondition = nextX >= 0;
    }
  } else {
    nextX = x;
    if(direction === 't') {
      nextY = y - 1;
      borderCondition = nextY >= 0;
    } else {
      nextY = y + 1;
      borderCondition = nextY < this.level.ySize;
    }
  }

  if(borderCondition) {
    nextGem = this.board.getGem(nextX, nextY);
    if(searchResult.frame === undefined) {
      searchResult.frame = nextGem.frame;
      searchResult.size = 1;
      this.bombMatch(searchResult, nextX, nextY, axis, direction);
    } else {
      if(searchResult.frame === nextGem.frame) {
        searchResult.size += 1;
        this.bombMatch(searchResult, nextX, nextY, axis, direction);
      } else {
        return;
      }
    }
  } else {
    return;
  }
}

Game.GemsMatches.prototype.defaultCheck = function() {
  var level = this.board.level,
      x, y, gem;

  this.checkTable = [];

  for(x = 0; x < level.xSize; x++) {
    this.checkTable[x] = [];
    for(y = 0; y < level.ySize; y++) {
      this.checkTable[x][y] = {};
    }
  }

  var currentFrame, hits = [];

  for(x = 0; x < level.xSize; x++) {
    currentFrame = undefined;
    hits = [];
    for(y = 0; y < level.ySize; y++) {
      gem = this.board.getGem(x, y);
      if(gem === null) { continue; }
      if(currentFrame === undefined) {
        currentFrame = gem.frame;
        hits.push(gem);
      } else if(currentFrame !== gem.frame) {
        currentFrame = gem.frame;
        this.pareseHits(hits);
        hits = [gem];
      } else {
        hits.push(gem);
      }

      if(y === level.ySize - 1) {
        this.pareseHits(hits);
      }
    }
  }
  for(y = 0; y < level.ySize; y++) {
    currentFrame = undefined;
    hits = [];
    for(x = 0; x < level.xSize; x++) {
      gem = this.board.getGem(x, y);
      if(gem === null) { continue; }
      if(currentFrame === undefined) {
        currentFrame = gem.frame;
        hits.push(gem);
      } else if(currentFrame !== gem.frame) {
        currentFrame = gem.frame;
        this.pareseHits(hits);
        hits = [gem];
      } else {
        hits.push(gem);
      }

      if(x === level.xSize - 1) {
        this.pareseHits(hits);
      }
    }
  }
}

Game.GemsMatches.prototype.pareseHits = function(hits) {
  var self = this;
  if(hits.length >= 3) {
    this.matches.push({ type: 'normal', hits: hits });
  }
}

