Game.LevelsDefaults = {
  board: [
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1]
  ],
  xSize: 7,
  ySize: 8
}
Game.Levels = {
  active: ['I'],
  I: {
    active: [1, 2, 3, 4, 5],
    bg: 'mineBg',
    1: {
      board: Game.LevelsDefaults.board,
      xSize: Game.LevelsDefaults.xSize,
      ySize: Game.LevelsDefaults.ySize,
      active: true,
      numberOfTiles: 7,
      win: {
        conditions: {
          score: 100
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
    },
    2: {
      board: Game.LevelsDefaults.board,
      xSize: Game.LevelsDefaults.xSize,
      ySize: Game.LevelsDefaults.ySize,
      active: true,
      numberOfTiles: 7,
      win: {
        conditions: {
          score: 200
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
    },
    3: {
      board: Game.LevelsDefaults.board,
      xSize: Game.LevelsDefaults.xSize,
      ySize: Game.LevelsDefaults.ySize,
      active: true,
      numberOfTiles: 8,
      win: {
        conditions: {
          score: 400
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
    },
    4: {
      board: Game.LevelsDefaults.board,
      xSize: Game.LevelsDefaults.xSize,
      ySize: Game.LevelsDefaults.ySize,
      active: true,
      numberOfTiles: 8,
      win: {
        conditions: {
          score: 800
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
    },
    5: {
      board: Game.LevelsDefaults.board,
      xSize: Game.LevelsDefaults.xSize,
      ySize: Game.LevelsDefaults.ySize,
      active: true,
      numberOfTiles: 7,
      win: {
        conditions: {
          score: 1000
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
    },
    6: {
      board: Game.LevelsDefaults.board,
      xSize: Game.LevelsDefaults.xSize,
      ySize: Game.LevelsDefaults.ySize,
      active: true,
      numberOfTiles: 7,
      win: {
        conditions: {
          score: 200
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
    },
    7: {
      board: Game.LevelsDefaults.board,
      xSize: Game.LevelsDefaults.xSize,
      ySize: Game.LevelsDefaults.ySize,
      active: true,
      numberOfTiles: 7,
      win: {
        conditions: {
          score: 200
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
    },
    8: {
      board: Game.LevelsDefaults.board,
      xSize: Game.LevelsDefaults.xSize,
      ySize: Game.LevelsDefaults.ySize,
      active: true,
      numberOfTiles: 7,
      win: {
        conditions: {
          score: 200
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
    },
    9: {
      board: Game.LevelsDefaults.board,
      xSize: Game.LevelsDefaults.xSize,
      ySize: Game.LevelsDefaults.ySize,
      active: true,
      numberOfTiles: 7,
      win: {
        conditions: {
          score: 200
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
    },
    10: {
      board: Game.LevelsDefaults.board,
      xSize: Game.LevelsDefaults.xSize,
      ySize: Game.LevelsDefaults.ySize,
      active: true,
      numberOfTiles: 7,
      win: {
        conditions: {
          score: 200
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
    }
  },
  II: {
    active: [1, 2],
    bg: 'factoryBg',
    1: {
      active: true,
      numberOfTiles: 6,
      xSize: 8,
      ySize: 8,
      win: {
        conditions: {
          score: 1000
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
      board: [
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1]
      ]
    },

    2: {
      active: true,
      numberOfTiles: 3,
      xSize: 8,
      ySize: 8,
      win: {
        conditions: {
          score: 1000
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
      board: [
        [1, 1, 1, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 1, 1, 1]
      ]
    }
  },
  III: {
    active: [1],
    bg: 'seaBg',
    1: {
      active: true,
      numberOfTiles: 7,
      xSize: 8,
      ySize: 10,
      win: {
        conditions: {
          score: 1000
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
      board: [
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1]
      ]
    }
  },
  IV: {
    active: [1],
    bg: 'seaBg',
    1: {
      active: true,
      numberOfTiles: 7,
      xSize: 8,
      ySize: 10,
      win: {
        conditions: {
          score: 1000
        },
        limitations: {
          time: 960
          //moves: 30
        }
      },
      board: [
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1]
      ]
    }
  }
}
