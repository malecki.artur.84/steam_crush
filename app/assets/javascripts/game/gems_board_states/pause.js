Game.GemsBoardStates.Pause = function(board) {
  this.board = board;
};

Game.GemsBoardStates.Pause.prototype = {
  update: function() {
  },

  entryAction: function() {

  },

  exitAction: function() {
  }
};

Game.GemsBoardStates.Idle.prototype.class = Game.GemsBoardStates.Idle;
