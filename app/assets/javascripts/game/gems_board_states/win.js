Game.GemsBoardStates.Win = function(board) {
  this.board = board;
};

Game.GemsBoardStates.Win.prototype = {
  update: function() {
  },

  entryAction: function() {
    var nextGame = new Game.Services.NextGame(Game.User.level, Game.User.sublevel);

    if(nextGame.isEndGame()) {
      this.board.endGameWindow.show();
    } else {
      localStorage.setItem("level_" + nextGame.level + "_" + nextGame.sublevel + "_avaiable", 1);
      Game.Sounds.playSound('win');
      this.board.winWindow.show();
    }
  },

  exitAction: function() {
  }
};

Game.GemsBoardStates.Win.prototype.constructor = Game.GemsBoardStates.Win;
