Game.GemsBoardStates.ClearState = function(board) {
  this.board = board;
};

Game.GemsBoardStates.ClearState.prototype = {
  update: function() {
    console.log("Clear State");
    this.board.clearSelectedGem();
    Game.Points.flush();
    Game.Points.persist(Game.User.level + "_" + Game.User.sublevel);
    return this.board.states.check_win;
  },

  entryAction: function() {
  },

  exitAction: function() {
  }
};

Game.GemsBoardStates.ClearState.prototype.class = Game.GemsBoardStates.ClearState;
