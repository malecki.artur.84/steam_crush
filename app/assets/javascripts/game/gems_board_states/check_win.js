Game.GemsBoardStates.CheckWin = function(board) {
  var winConfig = Game.Levels[Game.User.level][Game.User.sublevel].win;

  this.board = board;
  this.endGameService = new Game.Services.EndGame(board, winConfig);
};

Game.GemsBoardStates.CheckWin.prototype = {
  update: function() {
    if(this.endGameService.isWin()) {
      console.log("YOU WIN!!!!");
      return this.board.states.win;
    } else if(this.endGameService.isLost()) {
      console.log("... :(")
      this.board.game.state.start("MainMenu");
    } else {
      console.log("keep trying");
      return this.board.states.idle;
    }
  },

  entryAction: function() {
  },

  exitAction: function() {
  }
};

Game.GemsBoardStates.CheckWin.prototype.class = Game.GemsBoardStates.CheckWin;
