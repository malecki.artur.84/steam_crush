Game.GemsCrusher = function(board) {
  this.board = board;
}

Game.GemsCrusher.prototype = {
  run: function(setOfHits) {
    var self = this,
        crushCounterHash = {},
        index;

    setOfHits.forEach(function(gems) {
      var hits = gems.hits
      if(gems.type === 'normal' && hits.length >= 4) {
        index = Math.ceil((hits.length - 2)/2);
        hits[index].createBomb();
        hits[index] = undefined;
      }
      hits.forEach(function(gem) {
        var animation;

        if(gem) {
          animation = gem.animations.add('explosion', [1, 2, 3, 4, 5, 6, 7, 8, 9]);

          gem.loadTexture('explosion', 0);
          gem.destroying = true;

          animation.onComplete.add(function() {
            self.board.deadGemsGroup.add(arguments[0]);
            self.board.runRefill = true;
          }, self);
          Game.Sounds.playSound('explosion');
          animation.play(20, false);
        }
      });
      Game.Points.add(hits.length);
    });
  }
}

