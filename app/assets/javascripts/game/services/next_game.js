Game.Services.NextGame = function(level, sublevel) {
  var activeLevels = Game.Levels.active,
      levelIndex = Game.Levels.active.indexOf(level),
      activeSublevels = Game.Levels[level].active,
      sublevelIndex = activeSublevels.indexOf(sublevel);

  this.level = level;
  this.sublevel = sublevel;

  if(sublevelIndex + 1 >= activeSublevels.length) {
    if(levelIndex + 1 >= activeLevels.length) {
      this.level = undefined
    } else {
      this.level = activeLevels[levelIndex + 1];
      this.sublevel = Game.Levels[this.level].active[0];
    }
  } else {
    this.sublevel = activeSublevels[sublevelIndex + 1];
  }
}

Game.Services.NextGame.prototype = {
  isEndGame: function() {
    return this.level === undefined;
  }
};

Game.Services.NextGame.prototype.constructor = Game.Services.NextGame;
