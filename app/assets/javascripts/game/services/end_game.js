Game.Services.EndGame = function(board, winConfig) {
  this.winConditions = winConfig.conditions;
  this.limitations = winConfig.limitations;
};

Game.Services.EndGame.prototype = {
  isWin: function() {
    if("score" in this.winConditions) {
      return this.checkScore(this.winConditions.score);
    }
  },

  isLost: function() {
    return false;
  },

  checkScore: function(score) {
    if(Game.Points.get() >= score) {
      return true;
    } else {
      return false;
    }
  }
};

Game.Services.EndGame.prototype.class = Game.Services.EndGame;
