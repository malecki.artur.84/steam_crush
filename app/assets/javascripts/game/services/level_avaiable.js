Game.Services.LevelAvaiable = {
  checkLevel: function(level) {
    if(level === 'I'){
      return true;
    } else {
      return localStorage.getItem("level_" + level + "_" + Game.Levels[level].active[0] + "_avaiable") === '1';
    }
  },

  checkSublevel: function(level, sublevel) {
    if(sublevel === 1) {
      return true;
    } else {
      return localStorage.getItem("level_" + level + "_" + sublevel + "_avaiable") === '1';
    }
  }
};
