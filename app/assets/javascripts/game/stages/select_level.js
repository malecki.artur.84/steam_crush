Game.SelectLevel = {
  create: function() {
      var title, self = this, group, btnY = 0, bg;

      bg = this.game.add.image(0, 0, 'homeBg');

      Game.Views.default(this.game);

      group = this.game.add.group();

      Game.Levels.active.forEach(function(level, index) {
        var btnX, button;

        btnX = index * 200;
        if((index + 1) % 4 === 0) {
          btnY += 200;
          btnX = 0;
        }

        button = new Game.Views.LevelButton({
          game: this.game,
          parent: group,
          text: level,
          callback: function() {
            self.selectLevel(level);
          },
          context: this,
          active: Game.Services.LevelAvaiable.checkLevel(level)
        })
        button.group.x = btnX;
        button.group.y = btnY;
      });

      backButton = new Game.Views.Button({
        game: this.game,
        parent: group,
        text: 'Back',
        callback: this.back,
        context: this,
        size: 2
      });
      backButton.group.y = group.height + 100;
      backButton.group.x = (group.width / 2) - (backButton.group.width / 2)
      group.scale = new Phaser.Point(Game.scaleValue, Game.scaleValue);
      group.position = new PIXI.Point((this.game.width / 2) - (group.width / 2) , 50);

      bg.scale = new Phaser.Point(Game.scaleValue, Game.scaleValue);
      bg.x = -(bg.width / 2 - this.game.width / 2);
      bg.y = -(bg.height / 2 - this.game.height / 2);
  },
  render: function() {},

  update: function() {},

  selectLevel: function(level) {
    Game.User.level = level;
    this.state.start('SelectSublevel');
  },

  back: function() {
    this.state.start('MainMenu');
  }
}
