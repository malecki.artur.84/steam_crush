Game.Game = {
  timeCounter: 0,
  score: 0,

  create: function() {
    this.game.input.onDown.removeAll();

    Game.Views.background(this.game);
    this.time.events.loop(Phaser.Timer.SECOND, this.updateTimeText, this);

    this.gemsBoard = new Game.GemsBoard(this.game, Game.G.gemSizeWithSpacing);
    this.gameTopBar = new Game.Views.GameTopBar(this.game, this);

    Game.Views.footerInfo(this.game, {version: Game.version});
  },

  update: function() {
    if(!Game.G.paused) {
      this.gameTopBar.updateScoreText(Game.Points.points);
      this.gameTopBar.updateMaxScoreText(localStorage.getItem("level_" + Game.User.level + "_" + Game.User.sublevel + "_max"));
      this.gemsBoard.update();
    }
  },

  pauseGame: function(){
    this.gemsBoard.saveState();
    this.gemsBoard.setStateTo('pause');
    Game.G.paused = true;
  },

  unpauseGame: function(){
    Game.G.paused = false;
    this.gemsBoard.resumeSaveState();
  },

  updateTimeText: function() {
    this.timeCounter++;
    //this.gameTopBar.updateTimeText(this.timeCounter)
  },

  finishGame: function() {
    Game.paused = false;
    this.timeCounter = 0;
    Game.EndGame.score = this.score;
    this.score = 0;
    this.game.state.start('EndGame');
  }
}
