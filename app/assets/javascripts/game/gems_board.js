Game.GemsBoard = function(game, gemSize) {
  Phaser.Group.call(this, game);

  this.game            = game;
  this.gemSize         = gemSize;
  this.selectedGem     = undefined;
  this.gemsSwipe       = new Game.GemsSwipe(this);
  this.gemsDrop        = new Game.GemsDrop(this);
  this.gemsCrusher     = new Game.GemsCrusher(this);
  this.level           = Game.Levels[Game.User.level][Game.User.sublevel];
  this.gemsMatches     = new Game.GemsMatches(this);
  this.deadGemsGroup   = game.add.group();
  this.deadGemsGroup.x = -1000;
  this.deadGemsGroup.y = -1000;

  this.winWindow       = new Game.Views.WinWindow(this.game);
  this.endGameWindow   = new Game.Views.EndGameWindow(this.game);
  this.loseWindow      = new Game.Views.LoseWindow(this.game);

  this.populate();

  this.scale = new Phaser.Point(Game.scaleValue, Game.scaleValue);
  this.x     = (game.width / 2) - (this.width / 2);
  this.y     = 60;
  this.states = {
    find_pairs: new Game.GemsBoardStates.FindPairs(this),
    check_win: new Game.GemsBoardStates.CheckWin(this),
    clear_state: new Game.GemsBoardStates.ClearState(this),
    idle: new Game.GemsBoardStates.Idle(this),
    process_action: new Game.GemsBoardStates.ProcessAction(this),
    refill: new Game.GemsBoardStates.Refill(this),
    drop: new Game.GemsBoardStates.Drop(this),
    win: new Game.GemsBoardStates.Win(this),
    pause: new Game.GemsBoardStates.Pause(this)
  }
  this.state = this.states.find_pairs;

  Game.Points.clear();
}

Game.GemsBoard.prototype = Object.create(Phaser.Group.prototype);
Game.GemsBoard.prototype.constructor = Game.GemsBoard;

Game.GemsBoard.prototype.eachGem = function(func, context) {
  var level = this.level,
      x, y;
  for(y = 0; y < level.ySize; y++) {
    for(x = 0; x < level.xSize; x++) {
      if(level.board[y][x] === 1) {
        func.call(context, x, y);
      }
    }
  }
}


Game.GemsBoard.prototype.populate = function() {
  var gem, randomGem,
      self = this;

  var testBoard = [
    [1, 2, 3, 4, 5, 6, 7],
    [2, 3, 4, 5, 6, 7, 1],
    [1, 2, 3, 4, 5, 6, 7],
    [2, 3, 4, 5, 6, 7, 1],
    [0, 2, 3, 4, 5, 6, 1],
    [0, 3, 4, 5, 6, 7, 1],
    [0, 2, 3, 4, 5, 6, 1],
    [0, 2, 3, 5, 6, 7, 1]
  ];

  this.eachGem(function(x, y) {
    randomGem = Math.floor((Math.random() * self.level.numberOfTiles));
    //randomGem = testBoard[y][x];
    gem = new Game.Gem(this.game, this.gemSize * x, this.gemSize * y, 'elements', randomGem);
    gem.events.onInputDown.add(this.selectGem, this);
    this.add(gem);
  }, this);
}

Game.GemsBoard.prototype.setStateTo = function(state) {
  if(!Game.G.paused) {
    this.state.exitAction();
    this.state = this.states[state];
    this.state.entryAction();
  } else {
    this.savedState = this.states[state];
  }
}

Game.GemsBoard.prototype.saveState = function() {
  this.savedState = this.state;
}

Game.GemsBoard.prototype.resumeSaveState = function() {
  this.state.exitAction();
  this.state = this.savedState;
  this.state.entryAction();
  this.savedState = undefined;
}

Game.GemsBoard.prototype.update = function() {
  var state = this.state.update();

  if(!!state) {
    this.state.exitAction();
    this.state = state;
    this.state.entryAction();
  }

}

Game.GemsBoard.prototype.isSwapping = function() {
  return this.children.filter(function(gem) {
    return gem.swapping;
  }).length > 0;
}

Game.GemsBoard.prototype.isDropping = function() {
  return this.children.filter(function(gem) {
    return gem.dropping;
  }).length > 0;
}

Game.GemsBoard.prototype.selectGem = function(gem) {
  if(this.state instanceof Game.GemsBoardStates.Idle) {
    if(!!this.selectedGem) {
      this.selectedGem2 = gem;
      this.setStateTo('process_action');
    } else {
      this.selectedGem = gem;
      var frame = this.selectedGem.frame;
      this.selectedGem.loadTexture('elementsSelected');
      this.selectedGem.frame = frame;
    }
  }
}

Game.GemsBoard.prototype.isGemSelected = function() {
  return !!this.selectedGem;
}

/**
 * Converts absolute postion to relative one.
 * Includes position of the board and actual scale value.
 *
 * @this {Game.GemsBoard}
 * @params {number} posiion Position of the cursor
 * @params {string} cord Base on which coodinate
 */
Game.GemsBoard.prototype.convertToGemPosition = function(position, cord) {
  var p = (position - this[cord]) / Game.scaleValue;

  return Math.floor(p / this.gemSize) * this.gemSize;
}

Game.GemsBoard.prototype.getGemByPos = function(x, y) {
  var gemId = x + 'x' + y;

  return this.iterate('id', gemId, Phaser.Group.RETURN_CHILD);
}

Game.GemsBoard.prototype.getGem = function(x, y) {
  return this.getGemByPos(x * this.gemSize, y * this.gemSize)
}

Game.GemsBoard.prototype.getGemFrame = function(x, y) {
  var gem = this.getGem(x, y);

  if(gem) {
    return gem.frame;
  } else {
    return undefined;
  }
}

Game.GemsBoard.prototype.isEptyField = function(x, y) {
  return !this.getGem(x, y);
}

Game.GemsBoard.prototype.clearSelectedGem = function() {
  if(this.selectedGem) {
    var frame = this.selectedGem.frame;
    this.selectedGem.loadTexture('elements');
    this.selectedGem.frame = frame;
  }
  this.selectedGem = undefined;
  this.selectedGem2 = undefined;
}

