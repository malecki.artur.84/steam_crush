Game.Views.footerInfo = function(game, options) {
  var textStyle = { font: "12px Arial", fill: "#FFF" },
      group = game.add.group(),
      options = options || {},
      soundButton,
      text;

  group.x = 10;
  group.y = game.world.height - 37;

  soundButton = new Game.Views.SoundButton({game: game, parent: group, context: this});
  soundButton.group.x = (game.width - soundButton.group.width) - 20;
  soundButton.group.y = soundButton.group.y - 20;

  game.add.text(0, 0, "Created by gameboxZ.org\nVersion: " + options.version, textStyle, group);
};

