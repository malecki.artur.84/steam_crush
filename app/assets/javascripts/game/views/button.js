Game.Views.Button = function(options) {
  var game = options.game,
      label = options.text,
      parent = options.parent,
      callback = options.callback,
      context = options.context,
      size = options.size,
      button = new Phaser.Graphics(game, 0, 0),
      text, font, width, height;

  if(size === undefined) {
    size = 1;
  }

  if(size === 1) {
    font = {font: '20px Arial', fill: '#FFF'};
    width = 150;
    height = 50;
  } else if(size === 2) {
    font = {font: '36px Arial', fill: '#FFF'};
    width = 300;
    height = 100;
  }

  this.group = new Phaser.Group(game, parent);

  button.beginFill(0x000000);
  button.drawRect(0, 0, width, height);
  button.endFill();
  button.inputEnabled = true;
  this.group.add(button);
  text = game.add.text(0, 0, label, font, this. group);
  text.x = (button.width / 2) - (text.width / 2);
  text.y = (button.height / 2) - (text.height / 2);

  this.group.x = 0;
  this.group.y = 0;
  button.events.onInputDown.add(callback, context);
  button.events.onInputDown.add(function() {
    Game.Sounds.playSound('tap');
  }, this)
  //button.events.onInputOver.add(function() {text.fill = 'green'}, this);
  //button.events.onInputOut.add(function() {text.fill = 'white'}, this);
};
