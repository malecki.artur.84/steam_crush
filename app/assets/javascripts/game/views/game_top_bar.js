Game.Views.GameTopBar = function(game, context) {
  var config = Game.User.config(),
      button,
      self = this;

  this.game       = game;
  this.bg         = new Phaser.Graphics(game, 0, 0);
  this.pausePanel = new Game.Views.PauseWindow(game, {
    resumeCallback: this.unpauseGame,
    resumeContext: this,
    exitCallback: function() { game.state.start('MainMenu'); },
    exitContext: this
  });

  this.drawBG();

  this.context = context;

  this.scoreBox(1, "Score", "scoreText", "0");
  this.scoreBox(3, "Level", "levelText", Game.User.level + " - " + Game.User.sublevel);

  if(config.win.conditions.score !== undefined) {
    this.scoreBox(2, "Target", "targetText", config.win.conditions.score);
  }
  this.pauseButton(this.pauseGame, this);
};

Game.Views.GameTopBar.prototype.pauseButton = function(callback, context) {
  var group,
      gBg = new Phaser.Graphics(this.game, 0, 0),
      gPause1 = new Phaser.Graphics(this.game, 0, 0);
      gPause2 = new Phaser.Graphics(this.game, 0, 0);

  group = this.game.add.group();
  group.x = this.bg.width - 45;
  group.y = 5;
  group.width = 40;
  gBg.beginFill(0x000000);
  gBg.drawRect(0, 0, 40, 40);
  gBg.endFill();
  gPause1.beginFill(0xFFFFFF);
  gPause1.drawRect(5, 5, 10, 30);
  gPause1.endFill();
  gPause2.beginFill(0xFFFFFF);
  gPause2.drawRect(25, 5, 10, 30);
  gPause2.endFill();

  group.add(gBg);
  group.add(gPause1);
  group.add(gPause2);

  gBg.inputEnabled = true
  gBg.events.onInputDown.add(callback, context);
  gBg.events.onInputDown.add(function() {
    Game.Sounds.playSound('tap');
  }, this);
}

Game.Views.GameTopBar.prototype.scoreBox = function(position, title, scoreText, score) {
  var titleStyle = { font: "20px Arial", fill: "#000" },
      width = (this.bg.width - 50) / 3,
      scoreGroup;

  scoreGroup = this.game.add.group();
  scoreGroup.x = width * (position - 1);
  scoreGroup.y = 5;
  scoreGroup.width = width;
  scoreTitle = new Phaser.Text(this.game, 0, 0, title, titleStyle);
  scoreTitle.x = (width / 2) - (scoreTitle.width / 2);
  this[scoreText] = new Phaser.Text(this.game, 0, 22, score, { font: "20px Arial", fill: "#f6b020" });
  this[scoreText].x = (width / 2) - (this[scoreText].width / 2);
  scoreGroup.add(scoreTitle);
  scoreGroup.add(this[scoreText]);
}

Game.Views.GameTopBar.prototype.drawBG = function() {
  this.bg.beginFill(0x1F7A7A);
  this.bg.drawRect(0, 0, this.game.width, 50);
  this.bg.endFill();
  this.game.add.existing(this.bg);
}

Game.Views.GameTopBar.prototype.updateTimeText = function(time) {
  this.timeText.setText(time + "s");
};

Game.Views.GameTopBar.prototype.updateScoreText = function(score) {
  this.scoreText.setText(score);
  this.scoreText.x = (((this.bg.width - 50) / 3) / 2) - (this.scoreText.width / 2);
};

Game.Views.GameTopBar.prototype.updateMaxScoreText = function(score) {
  //this.maxScoreText.setText(score);
};

Game.Views.GameTopBar.prototype.pauseGame = function() {
  if(!Game.G.paused) {
    this.pausePanel.show();
    this.context.pauseGame();
  } else {
    this.unpauseGame();
  }
};

Game.Views.GameTopBar.prototype.unpauseGame = function() {
  this.pausePanel.hide();
  this.context.unpauseGame();
};
