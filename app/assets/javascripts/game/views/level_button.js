Game.Views.LevelButton = function(options) {
  var game = options.game,
      label = options.text,
      parent = options.parent,
      callback = options.callback,
      context = options.context,
      active = options.active,
      button = new Phaser.Graphics(game, 0, 0),
      text;

  this.group = new Phaser.Group(game, parent);

  if(active === true) {
    button.events.onInputDown.add(callback, context);
    button.events.onInputDown.add(function() {
      Game.Sounds.playSound('tap');
    }, context);
    button.beginFill(0x000000);
  } else {
    button.beginFill(0x6B6B61);
  }

  button.drawRect(0, 0, 100, 100);
  button.endFill();
  button.inputEnabled = true;
  this.group.add(button);
  text = game.add.text(0, 0, label, {font: '36px Arial', fill: '#FFF'}, this. group);
  text.x = (button.width / 2) - (text.width / 2);
  text.y = (button.height / 2) - (text.height / 2);

  this.group.x = 0;
  this.group.y = 0;

};
