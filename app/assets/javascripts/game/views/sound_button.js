Game.Views.SoundButton = function(options) {
  var game = options.game,
      parent = options.parent,
      context = options.context,
      button = new Phaser.Graphics(game, 0, 0),
      key = "sc_options_sound",
      font, width, height;

  font = {font: '20px Arial', fill: '#FFF'};
  width = 150;
  height = 50;


  this.group = new Phaser.Group(game, parent);

  button.beginFill(0x000000);
  button.drawRect(0, 0, width, height);
  button.endFill();
  button.inputEnabled = true;
  this.group.add(button);
  text = game.add.text(0, 0, "", font, this. group);

  if(localStorage.getItem(key) === "true") {
    text.text = "Sound ON"
  } else {
    text.text = "Sound OFF"
  }

  text.x = (button.width / 2) - (text.width / 2);
  text.y = (button.height / 2) - (text.height / 2);

  this.group.x = 0;
  this.group.y = 0;
  this.group.width = 150;
  button.events.onInputDown.add(function() {
    if(localStorage.getItem(key) === "true") {
      text.text = "Sound OFF";
      localStorage.setItem(key, false);
    } else {
      text.text = "Sound ON";
      localStorage.setItem(key, true);
      Game.Sounds.playSound('tap');
    }
  }, context);
};
