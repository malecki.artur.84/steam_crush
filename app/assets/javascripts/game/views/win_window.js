Game.Views.WinWindow = function(game) {
  this.bg = new Phaser.Graphics(game, 0, 0),
  this.game = game;
  this.group = this.game.add.group(),
  this.group.x = (this.game.world.width / 2) - 150;
  this.group.y = -210;

  this.bg.beginFill(0x1F7A7A);
  this.bg.drawRect(0, 0, 300, 200);
  this.bg.endFill();
  this.group.add(this.bg);

  nextButton = new Game.Views.Button({
    game: this.game,
    parent: this.group,
    text: 'Next Game',
    callback: function() {
      var nextGame = new Game.Services.NextGame(Game.User.level, Game.User.sublevel);

      Game.User.level = nextGame.level;
      Game.User.sublevel = nextGame.sublevel;

      this.state.start('PreGame');
    },
    context: this.game
  })
  nextButton.group.x = (this.bg.width - nextButton.group.width);
  nextButton.group.y = (this.bg.height - nextButton.group.height);

  reloadButton = new Game.Views.Button({
    game: this.game,
    parent: this.group,
    text: 'Repeat',
    callback: function() { this.state.start('PreGame'); },
    context: this.game
  })
  reloadButton.group.x = 0;
  reloadButton.group.y = (this.bg.height - reloadButton.group.height);
};

Game.Views.WinWindow.prototype = {
  show: function() {
    var textStyle = { font: "36px Arial", fill: "#FFF" },
        text1, text2;

    text1 = this.game.add.text(0, 20, "YOU WIN!!!", textStyle, this.group);
    text1.x = (150) - (text1.width / 2);

    text2 = this.game.add.text(0, 60, "Score: " + Game.Points.get(), textStyle, this.group);
    text2.x = (150) - (text2.width / 2);

    this.game.add.tween(this.group).to(
        { y: 100 }, 50, Phaser.Easing.Cubic.Out, true
    );

  }
}

Game.Views.WinWindow.prototype.constructor = Game.Views.WinWindow;
