Game.Views.LoseWindow = function(game) {
  this.bg = new Phaser.Graphics(game, 0, 0),
  this.game = game;
  this.group = this.game.add.group(),
  this.group.x = (this.game.world.width / 2) - 150;
  this.group.y = -210;

  this.bg.beginFill(0x1F7A7A);
  this.bg.drawRect(0, 0, 300, 200);
  this.bg.endFill();
  this.group.add(this.bg);

  mainMenu = new Game.Views.Button({
    game: this.game,
    parent: this.group,
    text: 'Menu',
    callback: function() {
      this.state.start('MainMenu');
    },
    context: this.game
  })
  mainMenu.group.x = (this.bg.width - mainMenu.group.width);
  mainMenu.group.y = (this.bg.height - mainMenu.group.height);

  reloadButton = new Game.Views.Button({
    game: this.game,
    parent: this.group,
    text: 'Repeat',
    callback: function() { this.state.start('PreGame'); },
    context: this.game
  })
  reloadButton.group.x = 0;
  reloadButton.group.y = (this.bg.height - reloadButton.group.height);
};

Game.Views.LoseWindow.prototype = {
  show: function() {
    var textStyle = { font: "36px Arial", fill: "#FFF" },
        text1, text2;

    text1 = this.game.add.text(0, 20, "YOU LOSE :(", textStyle, this.group);
    text1.x = (150) - (text1.width / 2);

    text2 = this.game.add.text(0, 60, "No more moves!", { font: "20px Arial", fill: "#FFF" }, this.group);
    text2.x = (150) - (text2.width / 2);

    text3 = this.game.add.text(0, 100, "Try again", { font: "20px Arial", fill: "#FFF" }, this.group);
    text3.x = (150) - (text3.width / 2);


    this.game.add.tween(this.group).to(
        { y: 100 }, 50, Phaser.Easing.Cubic.Out, true
    );

  }
}

Game.Views.WinWindow.prototype.constructor = Game.Views.WinWindow;
