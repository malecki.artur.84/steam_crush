Game.Views.PauseWindow = function(game, callbacks) {
  var textStyle = { font: "28px Arial", fill: "#FFF" },
      text;

  this.bg = new Phaser.Graphics(game, 0, 0),
  this.game = game;
  this.group = this.game.add.group(),
  this.group.x = (this.game.world.width / 2) - 100;
  this.group.y = -260;

  this.bg.beginFill(0x1F7A7A);
  this.bg.drawRect(0, 0, 200, 250);
  this.bg.endFill();
  this.group.add(this.bg);

  text = this.game.add.text(0, 20, "Menu", textStyle, this.group);
  text.x = 100 - (text.width / 2);

  resumeButton = new Game.Views.Button({
    game: this.game,
    parent: this.group,
    text: 'Resume',
    callback: callbacks.resumeCallback,
    context: callbacks.resumeContext
  })
  resumeButton.group.x = (this.bg.width / 2 - nextButton.group.width / 2);
  resumeButton.group.y = 60;

  reloadButton = new Game.Views.Button({
    game: this.game,
    parent: this.group,
    text: 'Repeat',
    callback: function() { this.state.start('PreGame'); },
    context: this.game
  })
  reloadButton.group.x = (this.bg.width / 2 - nextButton.group.width / 2);
  reloadButton.group.y = 120;

  exitButton = new Game.Views.Button({
    game: this.game,
    parent: this.group,
    text: 'Exit',
    callback: callbacks.exitCallback,
    context: callbacks.exitContext
  })
  exitButton.group.x = (this.bg.width / 2 - nextButton.group.width / 2);
  exitButton.group.y = 180;
};

Game.Views.PauseWindow.prototype = {
  show: function() {
    this.game.add.tween(this.group).to(
        { y: 100 }, 50, Phaser.Easing.Cubic.Out, true
    );

  },

  hide: function() {
    this.game.add.tween(this.group).to(
        { y: -260 }, 50, Phaser.Easing.Cubic.Out, true
    );
  }
}

Game.Views.PauseWindow.prototype.constructor = Game.Views.PauseWindow;
