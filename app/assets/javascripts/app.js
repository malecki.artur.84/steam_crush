var Game = {
  assetsPath: 'assets/',
  G: {
    menuItemFadeDelay: 75,
    paused: false,
    gemSize: 75,
    gemSpacing: 0,
    gemSizeWithSpacing: 75
  },
  User: {
    level: 1,
    sublevel: 1,
    config: function() {
      return Game.Levels[Game.User.level][Game.User.sublevel];
    },
    sound: true
  },
  GemsBoardStates: {},
  Views: {},
  Animations: {},
  Core: {},
  Services: {},
  Sounds: {
    playSound: function(sound) {
      if(localStorage.getItem("sc_options_sound") === "true") {
        this[sound].play();
      }
    }
  },
  version: '0.23.0',
  parentId: 'steam-crush-container',
  width: 640,
  height: 960,
  maxWidth: 1280,
  maxHeight: 1920,
  scaleValue: 1,
};

